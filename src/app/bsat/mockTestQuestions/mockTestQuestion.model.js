
const mongoose = require('mongoose');

const mockTestQuestionSchema = new mongoose.Schema({
  mockTestId: { 
    type: mongoose.Schema.Types.ObjectId,
      ref: "mockTest",
      required: false,
   },
  question: { type: String, required: false },
  questionImg: { type: String, required: false },
  optionList: { type: Array, required: false }
}, { timestamps: true });

module.exports = mongoose.model('mockTestQuestion', mockTestQuestionSchema);
