const mockTestQuestionModel = require("./mockTestQuestion.model");

const bulkUpload = async (req, res) => {
  try {
    const jsonData = req.body;

    if (!Array.isArray(jsonData)) {
      throw new Error("Invalid JSON format. The JSON data must be an array.");
    }

    const bulkInsertData = jsonData.map(
      ({
        mockTestId,
        question,
        optionValue1,
        isCorrect1,
        optionValue2,
        isCorrect2,
        optionValue3,
        isCorrect3,
        optionValue4,
        isCorrect4,
      }) => ({
        mockTestId,
        question,
        optionList: [
          {
            optionValue: optionValue1,
            optionId: generateUniqueId(),
            isCorrect: isCorrect1,
          },
          {
            optionValue: optionValue2,
            optionId: generateUniqueId(),
            isCorrect: isCorrect2,
          },
          {
            optionValue: optionValue3,
            optionId: generateUniqueId(),
            isCorrect: isCorrect3,
          },
          {
            optionValue: optionValue4,
            optionId: generateUniqueId(),
            isCorrect: isCorrect4,
          },
        ],
      })
    );

    const data = await mockTestQuestionModel.insertMany(bulkInsertData);

    res.status(200).json({
      data: data,
      dataCount: data.length,
      message: "mockTestQuestion uploaded successfully!",
      success: true,
      statusCode: 200,
    });
  } catch (err) {
    res.status(500).json({
      message:
        err.message ||
        "Some error occurred while uploading the mockTestQuestion.",
      success: false,
      statusCode: 500,
    });
  }
};

// Create Operation - Create mockTestQuestion
const createmockTestQuestion = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  const options = req.body.optionList;

  const optionList = options.map((o) => ({
    optionValue: o.optionValue,
    optionId: generateUniqueId(),
    isCorrect: o.isCorrect,
  }));

  const mockTestQuestion = new mockTestQuestionModel({
    ...req.body,
    optionList,
  });

  mockTestQuestion
    .save()
    .then((data) => {
      res.status(200).send({
        data: data,
        dataCount: 1,
        message: "mockTestQuestion created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while creating the mockTestQuestion.",
      });
    });
};

// Read Operation - Get all mockTestQuestion
const getAllmockTestQuestion = (req, res) => {
  mockTestQuestionModel
    .find()
    .populate({
      path: "mockTestId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTestQuestion fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "mockTestQuestion not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the mockTestQuestion.",
      });
    });
};

// Read Operation - Get all mockTestQuestion by Id
const getAllmockTestQuestionById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  mockTestQuestionModel
    .find(condition)
    .populate({
      path: "mockTestId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTestQuestion fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "mockTestQuestion not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the mockTestQuestion.",
      });
    });
};

// Read Operation - Get all mockTestQuestion by Id
const getmockTestQuestionById = (req, res) => {
  const mockTestId = req.params.id;
  const condition = { mockTestId: mockTestId };

  mockTestQuestionModel
    .find(condition)
    .populate({
      path: "mockTestId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTestQuestion fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "mockTestQuestion not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the mockTestQuestion.",
      });
    });
};

// Read Operation - Get a single mockTest by Id
const getMockTestQuestionByMockTestId = (req, res) => {
  const mockTestId = req.params.id;

  mockTestQuestionModel
    .find({ mockTestId })
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: 1,
          message: "mockTestQuestion fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "mockTestQuestion not found with classId=" + classId,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the StudyMaterial.",
      });
    });
};

// Update Operation - Update mockTestQuestion
const updatemockTestQuestion = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
      success: false,
      statusCode: 400,
    });
  }

  const id = req.params.id;

  const options = req.body.optionList || [];

  let updatedData;

  if (options.length > 0) {
    const optionList = options.map((o) => ({
      optionValue: o.optionValue,
      optionId: generateUniqueId(),
      isCorrect: o.isCorrect,
    }));

    updatedData = {
      ...req.body,
      optionList,
    };
  } else {
    updatedData = {
      ...req.body
    };
  }

  mockTestQuestionModel
    .findByIdAndUpdate(id, updatedData, { new: true, useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "mockTestQuestion was updated successfully.",
          success: true,
          statusCode: 200,
          data: data,
        });
      } else {
        res.status(404).send({
          message: `Cannot update mockTestQuestion with ID=${id}. Maybe mockTestQuestion was not found!`,
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating mockTestQuestion with ID=${id}`,
        success: false,
        statusCode: 500,
        error: err.message,
      });
    });
};


// A simple function to generate a unique ID
function generateUniqueId() {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
}

// Delete Operation - Delete mockTestQuestion
const deletemockTestQuestion = (req, res) => {
  const id = req.params.id;

  mockTestQuestionModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "mockTestQuestion was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete mockTestQuestion with ID=" +
            id +
            ". Maybe mockTestQuestion was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createmockTestQuestion,
  getAllmockTestQuestion,
  getAllmockTestQuestionById,
  getmockTestQuestionById,
  updatemockTestQuestion,
  deletemockTestQuestion,
  getMockTestQuestionByMockTestId,
  bulkUpload,
};
