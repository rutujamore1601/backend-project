
const mongoose = require('mongoose');

const submitmockTestSchema = new mongoose.Schema({
  userId: { 
    type: mongoose.Schema.Types.ObjectId,
      ref: "studentRegistration",
      required: false,
   },
  mockTestId: { 
    type: mongoose.Schema.Types.ObjectId,
      ref: "mockTest",
      required: false,
   },
  answerSheet: [
    {
      questionId:{ 
        type: mongoose.Schema.Types.ObjectId,
          ref: "mockTestQuestion",
          required: false,
       },
       answerId:{ type: String, required: false },
       isCorrect:{ type: Boolean, required: false },
       queIndex:{ type: Number, required: false }
    }
  ],
  totalQuestion:{ type: Number, required: false },
  attemptedQuestion:{ type: Number, required: false },
  unAttemptedQuestion:{ type: Number, required: false }, 
  correctAnswer:{ type: Number, required: false },
  wrongAnswer:{ type: Number, required: false },
  negativeMarks:{ type: Number, required: false },
  totalMarks:{ type: Number, required: false },
  obtainedMarks:{ type: Number, required: false },
  testResult:{ type: String, required: false },
  obtainedPercentage:{ type: Number, required: false },
  correctQuestionNumbers:{ type: Array, required: false },
  wrongQuestionNumbers:{ type: Array, required: false },
}, { timestamps: true });

module.exports = mongoose.model('submitmockTest', submitmockTestSchema);
