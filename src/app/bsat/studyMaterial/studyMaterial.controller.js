
const StudyMaterialModel = require('./studyMaterial.model');
const StudentRegistrationModel = require('./../studentRegistration/studentRegistration.model');

  // Create Operation - Create StudyMaterial
  const createStudyMaterial = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const StudyMaterial = new StudyMaterialModel(req.body);
    StudyMaterial
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "StudyMaterial created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the StudyMaterial.",
        });
      });
  };

  // Read Operation - Get all StudyMaterial
  const getAllStudyMaterial = (req, res) => {
    StudyMaterialModel.find()
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "StudyMaterial fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "StudyMaterial not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the StudyMaterial.",
        });
      });
  };

  // Read Operation - Get all StudyMaterial by Id 
  const getAllStudyMaterialById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    StudyMaterialModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "StudyMaterial fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "StudyMaterial not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the StudyMaterial.",
        });
      });
  };

  // Read Operation - Get a single StudyMaterial by Id
  const getStudyMaterialById = (req, res) => {
    const id = req.params.id;
    StudyMaterialModel.findById(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "StudyMaterial fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'StudyMaterial not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the StudyMaterial.",
        });
      });
  };

   // Read Operation - Get a single StudyMaterial by Id
   const getStudyMaterialByClassId = (req, res) => {
    const classId = req.params.classId;
    const medium = req.params.medium;
    const condition = {classId: classId, medium: medium};
  
    StudyMaterialModel.findOne(condition )
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: 1,
            message: "StudyMaterial fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'StudyMaterial not found with classId=' + classId,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the StudyMaterial.",
        });
      });
  };
  
  // Update Operation - Update StudyMaterial
 const updateStudyMaterial = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  StudyMaterialModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "StudyMaterial was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update StudyMaterial with order ID=' + id + '. Maybe StudyMaterial was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating StudyMaterial with ID=" + id,
      });
    });
};


// Delete Operation - Delete StudyMaterial
  const deleteStudyMaterial = (req, res) => {
    const id = req.params.id;
  
    StudyMaterialModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "StudyMaterial was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete StudyMaterial with ID=' + id + '. Maybe StudyMaterial was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createStudyMaterial,
  getAllStudyMaterial,
  getAllStudyMaterialById,
  getStudyMaterialById,
  updateStudyMaterial,
  deleteStudyMaterial,
  getStudyMaterialByClassId
};
