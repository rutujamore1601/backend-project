
const router = require("express").Router();
const submitBsatExamTestController = require('./submitBsatExamTest.controller');

// Create Operation - Create submitBsatExamTest
router.post('/createSubmitBsatExamTest', submitBsatExamTestController.createSubmitBsatExamTest);

// Read Operation - Get all submitBsatExamTest
router.get('/getAllSubmitBsatExamTest', submitBsatExamTestController.getAllSubmitBsatExamTest);

// Read Operation - Get all submitBsatExamTest by Id 
router.get("/getAllSubmitBsatExamTestById/:id", submitBsatExamTestController.getAllSubmitBsatExamTestById);

// Read Operation - Get a single submitBsatExamTest by Id
router.get('/getSubmitBsatExamTestById/:id', submitBsatExamTestController.getSubmitBsatExamTestById);

// Update Operation - Update submitBsatExamTest
router.put('/updateSubmitBsatExamTest/:id', submitBsatExamTestController.updateSubmitBsatExamTest);

// Delete Operation - Delete submitBsatExamTest
router.delete('/deleteSubmitBsatExamTest/:id', submitBsatExamTestController.deleteSubmitBsatExamTest);

module.exports = router;
