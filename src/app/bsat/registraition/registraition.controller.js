
const registraitionModel = require('./registraition.model');

  // Create Operation - Create registraition
  const createRegistraition = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
    const DOB = new Date(req.body.DOB);
  
    const currentDate = new Date();
    const age = currentDate.getFullYear() - DOB.getFullYear();
    req.body.age = age;

    const fullNameInitial = req.body.name.slice(0, 3);
    const capitalizedInitial = fullNameInitial.toUpperCase();
    const coordinatorCode = capitalizedInitial + '-' + req.body.mobileNo.toString();
    req.body.coordinatorCode = coordinatorCode;

    const registration = new registraitionModel(req.body);
  
    registration
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "Registration created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the registration.",
        });
      });
  };

  // Read Operation - Get all registraition
  const getAllRegistraition = async (req, res) => {
    try {
      const page = parseInt(req.body.page) || 1;
      const limit = parseInt(req.body.limit) || 25;
      const query = buildQuery(req.body);
  
      const [data, totalData] = await Promise.all([
        registraitionModel
          .find(query)
          .lean()
          .skip((page - 1) * limit)
          .limit(limit),
        registraitionModel.countDocuments(query),
      ]);
  
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          message: "Registration Data fetched successfully!",
          success: true,
          statusCode: 200,
          totalData: totalData,
        });
      } else {
        res.status(200).send({
          data: [],
          message: "Registration Data not found!",
          success: false,
          statusCode: 200,
          totalData: 0,
        });
      }
    } catch (err) {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving the Registration Data.",
      });
    }
  };

  // Read Operation - Get all registraition by Id 
  const getAllRegistraitionById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    registraitionModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "registraition fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "registraition not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the registraition.",
        });
      });
  };

  // Read Operation - Get a single registraition by Id
  const getRegistraitionById = (req, res) => {
    const id = req.params.id;
    registraitionModel.findById(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "registraition fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'registraition not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the registraition.",
        });
      });
  };

  // Update Operation - Update registraition
 const updateRegistraition = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  const DOB = new Date(req.body.DOB);
  
    const currentDate = new Date();
    const age = currentDate.getFullYear() - DOB.getFullYear();
    req.body.age = age;

  registraitionModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "registraition was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update registraition with order ID=' + id + '. Maybe registraition was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating registraition with ID=" + id,
      });
    });
};

// Delete Operation - Delete registraition
  const deleteRegistraition = (req, res) => {
    const id = req.params.id;
  
    registraitionModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "registraition was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete registraition with ID=' + id + '. Maybe registraition was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createRegistraition,
  getAllRegistraition,
  getAllRegistraitionById,
  getRegistraitionById,
  updateRegistraition,
  deleteRegistraition
};

// Function to build the query dynamically
const buildQuery = (body) => {
  const filters = {
    name: "name",
    coordinatorCode: "coordinatorCode",
    mobileNo: "mobileNo"
  };

  const query = {};

  for (const key in filters) {
    if (body[filters[key]] && body[filters[key]].length > 0) {
      if (key === "name") {
        query[key] = { $regex: body[filters[key]], $options: "i" };
      } else if (key === "coordinatorCode") {
        query[key] = { $regex: body[filters[key]], $options: "i" };
      } else if (key === "mobileNo") {
      query[key] = { $regex: body[filters[key]], $options: "i" };
    } else {
        query[key] = { $in: body[filters[key]] };
      }
    }
  }
  return query;
};
