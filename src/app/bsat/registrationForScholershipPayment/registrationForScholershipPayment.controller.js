const registrationForScholershipPaymentModel = require("./registrationForScholershipPayment.model");
const razorpayPaymentUtil = require("../../../shared/utils/razorpayPayment.util");

const proceedPayment = (req, res) => {
  razorpayPaymentUtil.proceedPayment(req, res);
};

// Create Operation - Create registrationForScholershipPayment
const createRegistrationForScholershipPayment = (req, res) => {
  if (razorpayPaymentUtil.verifyPaymentSignature(req.body.razorpayResData)) {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }

    const registrationForScholershipPayment =
      new registrationForScholershipPaymentModel(req.body);
    if (req.body.paymentStatus === "paid") {
      registrationForScholershipPayment.purchase = true;
    } else {
      registrationForScholershipPayment.purchase = false;
    }

    registrationForScholershipPayment
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "registrationForScholershipPayment created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message ||
            "Some error occurred while creating the registrationForScholershipPayment.",
        });
      });
  } else {
    console.log("Signature verification failed.");
  }
};

// Read Operation - Get all registrationForScholershipPayment
const getAllRegistrationForScholershipPayment = (req, res) => {
  registrationForScholershipPaymentModel
    .find()
    .populate({
      path: "userId",
    })
    .populate({
      path: "bsatExamRegistrationId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "registrationForScholershipPayment fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "registrationForScholershipPayment not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the registrationForScholershipPayment.",
      });
    });
};

// Read Operation - Get all registrationForScholershipPayment by Id
const getAllRegistrationForScholershipPaymentById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  registrationForScholershipPaymentModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "registrationForScholershipPayment fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "registrationForScholershipPayment not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the registrationForScholershipPayment.",
      });
    });
};

// Read Operation - Get a single registrationForScholershipPayment by Id
const getRegistrationForScholershipPaymentById = (req, res) => {
  const id = req.params.id;
  registrationForScholershipPaymentModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "registrationForScholershipPayment fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "registrationForScholershipPayment not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the registrationForScholershipPayment.",
      });
    });
};

// Update Operation - Update registrationForScholershipPayment
const updateRegistrationForScholershipPayment = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  registrationForScholershipPaymentModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message:
            "registrationForScholershipPayment was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update registrationForScholershipPayment with order ID=" +
            id +
            ". Maybe registrationForScholershipPayment was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          "Error updating registrationForScholershipPayment with ID=" + id,
      });
    });
};

// Delete Operation - Delete registrationForScholershipPayment
const deleteRegistrationForScholershipPayment = (req, res) => {
  const id = req.params.id;

  registrationForScholershipPaymentModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message:
            "registrationForScholershipPayment was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete registrationForScholershipPayment with ID=" +
            id +
            ". Maybe registrationForScholershipPayment was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createRegistrationForScholershipPayment,
  getAllRegistrationForScholershipPayment,
  getAllRegistrationForScholershipPaymentById,
  getRegistrationForScholershipPaymentById,
  updateRegistrationForScholershipPayment,
  deleteRegistrationForScholershipPayment,
  proceedPayment,
};
