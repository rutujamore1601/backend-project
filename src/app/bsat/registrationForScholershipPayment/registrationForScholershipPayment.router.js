
const router = require("express").Router();
const registrationForScholershipPaymentController = require('./registrationForScholershipPayment.controller');

router.post('/proceedPayment', registrationForScholershipPaymentController.proceedPayment);

// Create Operation - Create registrationForScholershipPayment
router.post('/createRegistrationForScholershipPayment', registrationForScholershipPaymentController.createRegistrationForScholershipPayment);

// Read Operation - Get all registrationForScholershipPayment
router.get('/getAllRegistrationForScholershipPayment', registrationForScholershipPaymentController.getAllRegistrationForScholershipPayment);

// Read Operation - Get all registrationForScholershipPayment by Id 
router.get("/getAllRegistrationForScholershipPaymentById/:id", registrationForScholershipPaymentController.getAllRegistrationForScholershipPaymentById);

// Read Operation - Get a single registrationForScholershipPayment by Id
router.get('/getRegistrationForScholershipPaymentById/:id', registrationForScholershipPaymentController.getRegistrationForScholershipPaymentById);

// Update Operation - Update registrationForScholershipPayment
router.put('/updateRegistrationForScholershipPayment/:id', registrationForScholershipPaymentController.updateRegistrationForScholershipPayment);

// Delete Operation - Delete registrationForScholershipPayment
router.delete('/deleteRegistrationForScholershipPayment/:id', registrationForScholershipPaymentController.deleteRegistrationForScholershipPayment);

module.exports = router;
