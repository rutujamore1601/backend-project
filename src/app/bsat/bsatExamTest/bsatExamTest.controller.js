
const bsatExamTestModel = require('./bsatExamTest.model');
const bsatExamTestResultModel = require('./../submitBsatExamTest/submitBsatExamTest.model');
const bsatExamQuestionModel = require('./../bsatExamQuestion/bsatExamQuestion.model');

  // Create Operation - Create bsatExamTest
  const createBsatExamTest = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const bsatExamTest = new bsatExamTestModel(req.body);
    bsatExamTest
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "bsatExamTest created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the bsatExamTest.",
        });
      });
  };

  // Read Operation - Get all bsatExamTest
  const getAllBsatExamTest = (req, res) => {
    bsatExamTestModel.find()
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "bsatExamTest fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "bsatExamTest not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the bsatExamTest.",
        });
      });
  };

  // Read Operation - Get all bsatExamTest by Id 
  const getAllBsatExamTestById  =  (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    bsatExamTestModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "bsatExamTest fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "bsatExamTest not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the bsatExamTest.",
        });
      });
  };

  // Read Operation - Get a single bsatExamTest by Id
  const getBsatExamTestByClassId = async (req, res) => {
    try {
      const classId = req.params.classId;
      const medium = req.params.medium;
      const condition = {classId: classId, medium: medium};
      const userId = req.params.userId;

      const data = await bsatExamTestModel.find(condition).lean();
      console.log('data', data)

      const results = await Promise.all(data.map(async (element) => {
          const resultInfo = await bsatExamTestResultModel.findOne({
              $and: [
                  { bsatExamTestId: element._id },
                  { userId: userId }
              ]
          }).sort({ createdAt: -1 });

          
          if (resultInfo) {
            element.isAttempted = true;
            element.resultDetails = resultInfo;
          } else {
            element.isAttempted = false;
            element.resultDetails = {}
            element.resultDetails['totalQuestion'] = await bsatExamQuestionModel.countDocuments({ bsatExamTestId: element._id }).exec();
          }

          return element;
      }
      ));

      if (data.length > 0) {
          res.status(200).send({
              data: results,
              dataCount: data.length,
              message: "bsat Exam test fetch successfully!",
              success: true,
              statusCode: 200,
          });
      } else {
          res.status(200).send({
              data: {},
              dataCount: 0,
              message: `bsat Exam test not found with classId=${classId}`,
              status: false,
              statusCode: 200,
          });
      }
    } catch (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the bsat Exam test.",
        });
    }
  };

  // Update Operation - Update bsatExamTest
 const updateBsatExamTest = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  bsatExamTestModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "bsatExamTest was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update bsatExamTest with order ID=' + id + '. Maybe bsatExamTest was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating bsatExamTest with ID=" + id,
      });
    });
};


// Delete Operation - Delete bsatExamTest
  const deleteBsatExamTest = (req, res) => {
    const id = req.params.id;
  
    bsatExamTestModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "bsatExamTest was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete bsatExamTest with ID=' + id + '. Maybe bsatExamTest was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createBsatExamTest,
  getAllBsatExamTest,
  getAllBsatExamTestById,
  getBsatExamTestByClassId,
  updateBsatExamTest,
  deleteBsatExamTest
};
