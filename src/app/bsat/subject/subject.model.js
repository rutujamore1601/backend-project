
const mongoose = require('mongoose');

const subjectSchema = new mongoose.Schema({
  subjectName: { type: String, required: false },
  subjectImg: { type: String, required: false },
  classId: { type: String, required: false },
}, { timestamps: true });

module.exports = mongoose.model('subject', subjectSchema);
