
const chapterModel = require('./chapter.model');

  // Create Operation - Create chapter
  const createChapter = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const chapter = new chapterModel(req.body);
    chapter
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "chapter created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the chapter.",
        });
      });
  };

  // Read Operation - Get all chapter
  const getAllChapter = (req, res) => {
    chapterModel.find()
    .populate({
      path: "subject",
    })
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "chapter fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "chapter not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the chapter.",
        });
      });
  };

  // Read Operation - Get all chapter by Id 
  const getAllChapterById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    chapterModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "chapter fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "chapter not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the chapter.",
        });
      });
  };

  // Read Operation - Get a single chapter by Id
  const getChapterById = (req, res) => {
    const id = req.params.id;
    chapterModel.findById(id)
    .populate({
      path: "subject",
    })
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "chapter fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'chapter not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the chapter.",
        });
      });
  };

   // Read Operation - Get a single subject by ClassId
   const getChapterBySubject = (req, res) => {
    const subject = req.params.subject;
    chapterModel.findOne({ subject: subject })
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: 1,
            message: "chapter fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'chapter not found with ID=' + subject,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the chapter.",
        });
      });
};

  // Update Operation - Update chapter
 const updateChapter = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  chapterModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "chapter was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update chapter with order ID=' + id + '. Maybe chapter was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating chapter with ID=" + id,
      });
    });
};

// Delete Operation - Delete chapter
  const deleteChapter = (req, res) => {
    const id = req.params.id;
  
    chapterModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "chapter was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete chapter with ID=' + id + '. Maybe chapter was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createChapter,
  getAllChapter,
  getAllChapterById,
  getChapterById,
  updateChapter,
  deleteChapter,
  getChapterBySubject
};
